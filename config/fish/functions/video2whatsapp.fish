function video2whatsapp
  ffmpeg -i $argv[1] -c:v libx264 -profile:v baseline -level 3.0 -pix_fmt yuv420p -s 1920x1080 "$argv[1].wp.mp4"
end
