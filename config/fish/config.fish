set -g theme_date_format "+%H:%M"
set -g theme_powerline_fonts no
set -g theme_nerd_fonts yes
set -g theme_newline_cursor yes

set -xg ENVIRONNEMENT_CONFIG LOCAL_CALEB
set -x VIRTUAL_ENV_DISABLE_PROMPT 1

fenv source ~/.profile

set -x N_PREFIX "$HOME/.npm-global"; contains "$N_PREFIX/bin" $PATH; or set -a PATH "$N_PREFIX/bin"  # Added by n-install (see http://git.io/n-install-repo).

set -xg DOKKU_PORT 3022
set -xg DOKKU_HOST dokku.docker
alias dokku 'bash $HOME/.dokku/contrib/dokku_client.sh'

# Scaleway CLI autocomplete initialization.
eval (scw autocomplete script shell=fish)

# Created by `pipx` on 2023-07-13 19:07:02
set PATH $PATH /home/calev/.local/bin

pyenv init - | source

set PATH /home/calev/.meteor $PATH
